﻿// Prints the name of the object camera is directly looking at
using UnityEngine;
using System.Collections;

public class RayCastSpawner : MonoBehaviour
{
    public Camera camera;
	public GameObject Projectile;

	public GameObject RandomCurveObject;
	private RandomDistribution Randomizer;

	public GameObject Tunnel;
	
	public LayerMask MyLayerMask;


    void Start()
    {
        camera = GetComponent<Camera>();
		iTween.MoveTo(gameObject,iTween.Hash("z",2600,"time",620));

		Randomizer = RandomCurveObject.GetComponent<RandomDistribution>();
    }


	void TestingDebugLog() { 
		Debug.Log("Does this work???");
	}

    void FixedUpdate()
    {
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, MyLayerMask)) {
        	print("I'm looking at " + hit.transform.name);

				GameObject clone;
				clone = Instantiate(Projectile, hit.point, transform.rotation);// as Rigidbody;
				//clone.transform.parent = Tunnel.transform;
				clone.transform.localScale = RandomScale();
				//clone.transform.position = hit.point;
				Destroy(clone, 5);
				



				//instantiatedObject.transform.position = hit.point;
			//}
			 
        } else {
            print("I'm looking at nothing!");
		}

		//Debug.Log(Randomizer.RandomFloat());

    }

	private Vector3 RandomScale()  {

		//float _randomFloat = Random.Range(.05f, 2.0f);
		float _randomFloat = Randomizer.RandomFloat();//Generate random float from 

		Vector3 _randomScale = new Vector3(_randomFloat,_randomFloat,_randomFloat);

		return _randomScale;
	}
}